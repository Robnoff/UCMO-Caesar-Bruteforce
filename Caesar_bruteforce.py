# This script is used to try and bruteforce a caesar cipher without knowing
# without knowing the amount of shifts.
# The script will try all the possibilities and print the text

# Date: 08/22/2018
# Author: Visser, Robert

ciphertext = 'Jvunyhabshapvuz! Fvb qbza kljyfwalk h jpwolyalea. Aopz pz jhlzhy jpwoly aoha zopmaz slaalyz if zlclu.'

# Loop through every shift and read every text in line
for shift in range(0,25):
    decrypted_cipher = ''

    # For every word in the text, replace the letter with the shifted character
    for lines in ciphertext:
        shifted_char = (ord(lines)+shift)
        
        # Check if the character in question in an uppercase or lowercase letter
        # The shifting of characters is to 'wrap around' the beginning again 
        if lines.isupper():
            if shifted_char > ord('Z'):
                shifted_char -= 26
            elif shifted_char < ord('A'):
                shifted_char += 26

        if lines.islower():
            if shifted_char > ord('z'):
                shifted_char -= 26
            elif shifted_char < ord('a'):
                shifted_char += 26

        # If the character is a space, substract the shift to keep te original value
        if lines.isspace():
            shifted_char -= shift

        # Add the characters to the string to make a sentence
        decrypted_cipher = decrypted_cipher + chr(shifted_char)
        
    print('Shifted {0} times, result: \n {1} \n'.format(shift, decrypted_cipher))   